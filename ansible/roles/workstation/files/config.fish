#!/usr/bin/env fish

starship init fish | source
zoxide init fish | source
direnv hook fish | source

set --export --prepend PATH $HOME/.cargo/bin
