#!/usr/bin/fish

set --local GPG_AGENT_SOCKET "$XDG_RUNTIME_DIR/gnupg/S.gpg-agent.ssh"
set --export SSH_AUTH_SOCK $GPG_AGENT_SOCKET

function start_gpg_agent
    gpg-agent --daemon >/dev/null 2>&1
    if test $status -ne 0
        echo "Failed to start gpg-agent"
        exit $status
    end

    set --export GPG_TTY (tty)
end

function connect_gpg_agent
    gpg-connect-agent updatestartuptty /bye >/dev/null 2>&1
    if test $status -ne 0
        echo "Failed to connect to gpg-agent"
        exit $status
    end
end

if test (id -u) -ne 0
    if test -S $GPG_AGENT_SOCKET
        pkill ssh
        pkill gpg-agent
        start_gpg_agent
    end

    connect_gpg_agent
end
