terraform {
  required_version = "~> 1.1.0"

  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.11"
    }
  }

}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_pool" "this" {
  name = "workstation-test"
  type = "dir"
  path = "${path.module}/workstation-test-pool"
}

resource "libvirt_volume" "this" {
  name   = "workstation"
  pool   = libvirt_pool.this.name
  source = "${path.module}/../packer/fedora_test/packer-fedora"
  format = "qcow2"
}

resource "libvirt_network" "internet" {
  name   = "internet-access"
  mode   = "bridge"
  bridge = "virbr0"
}

resource "libvirt_network" "ansible" {
  name      = "local-access"
  mode      = "nat"
  addresses = ["10.0.0.0/24"]
}

resource "libvirt_domain" "this" {
  name   = "workstation"
  memory = "4096"
  vcpu   = 4

  disk {
    volume_id = libvirt_volume.this.id
  }

  network_interface {
    network_id     = libvirt_network.internet.id
    hostname       = "workstation"
    wait_for_lease = true
  }

  network_interface {
    network_id     = libvirt_network.ansible.id
    addresses      = ["10.0.0.2"]
    wait_for_lease = true
  }
}
