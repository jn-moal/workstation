variable "iso_url" {
  type    = string
  default = "https://download.fedoraproject.org/pub/fedora/linux/releases/35/Server/x86_64/iso/Fedora-Server-netinst-x86_64-35-1.2.iso"
}

variable "iso_checksum" {
  type    = string
  default = "sha256:dd35f955dd5a7054213a0098c6ee737ff116aa3090fc6dbfe89d424b5c3552dd"
}

variable "disk_size" {
  type    = string
  default = "50000M"
}

source "qemu" "fedora" {
  iso_url          = var.iso_url
  iso_checksum     = var.iso_checksum
  output_directory = "fedora_test"
  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
  format           = "qcow2"
  accelerator      = "kvm"

  http_directory = "http"

  ssh_username     = "jnmoal"
  ssh_password     = "packer"
  ssh_wait_timeout = "60m"

  boot_wait    = "2s"
  boot_command = ["<tab> inst.text inst.lang=en_US inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/kickstart-fedora.conf<enter><wait>"]

  cpus           = 4
  memory         = 4096
  disk_size      = var.disk_size
  disk_interface = "virtio"
}

build {
  name    = "fedora"
  sources = ["source.qemu.fedora"]
}
